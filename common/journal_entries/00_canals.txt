﻿je_dhal_nikhuvad_survey = {
	icon = "gfx/interface/icons/event_icons/event_map.dds"

	group = je_group_foreign_affairs

	on_monthly_pulse = {
		effect = {
			change_variable = {
				name = survey_dhal_nikhuvad_taken
				add = 1
			}
		}
	}

	complete = {
		scope:journal_entry = { is_goal_complete = yes }
	}

	on_complete = {
		remove_modifier = modifier_surveying_dhal_nikhuvad
		trigger_event = { id = canal_events.100 }
	}

	current_value = {
		value = root.var:survey_dhal_nikhuvad_taken
	}

	goal_add_value = {
		value = 12
	}

	invalid = {
		OR = {
			NOT = { has_variable = survey_dhal_nikhuvad_taken }
			any_state = {
				state_region = s:STATE_BEERAGGA
				NOT = { owner = ROOT }
				any_scope_building = {
					is_building_type = building_dhal_nikhuvad_canal
				}
			}
		}
	}

	on_invalid = {
		remove_variable = survey_dhal_nikhuvad_taken
		remove_modifier = modifier_surveying_suez
	}

	progressbar = yes

	weight = 10
	should_be_pinned_by_default = yes
}

je_dhal_nikhuvad_canal = {
	icon = "gfx/interface/icons/event_icons/event_map.dds"

	group = je_group_foreign_affairs

	complete = {
		any_scope_state = {
			state_region = s:STATE_BEERAGGA
			any_scope_building = {
				is_building_type = building_dhal_nikhuvad_canal
				occupancy >= 0.9
			}
		}
	}

	on_complete = {
		set_global_variable = dhal_nikhuvad_canal_complete
	}
	should_be_pinned_by_default = yes
}

# je_panama_survey = {
# 	icon = "gfx/interface/icons/event_icons/event_map.dds"

# 	group = je_group_foreign_affairs

# 	on_monthly_pulse = {
# 		effect = {
# 			change_variable = {
# 				name = survey_panama_taken
# 				add = 1
# 			}
# 		}
# 	}

# 	complete = {
# 		scope:journal_entry = { is_goal_complete = yes }
# 	}

# 	on_complete = {
# 		remove_modifier = modifier_surveying_panama
# 		trigger_event = { id = canal_events.101 }
# 	}

# 	current_value = {
# 		value = root.var:survey_panama_taken
# 	}

# 	goal_add_value = {
# 		value = 12
# 	}

# 	invalid = { # If the Panama Canal is being built and you're not the one building it, you've blown it, chief.
# 		OR = {
# 			NOT = { has_variable = survey_panama_taken }
# 			any_state = {
# 				state_region = s:STATE_PANAMA
# 				NOT = { owner = ROOT }
# 				any_scope_building = {
# 					is_building_type = building_panama_canal
# 				}
# 			}
# 		}
# 	}

# 	on_invalid = {
# 		remove_variable = survey_suez_taken
# 		remove_modifier = modifier_surveying_suez
# 	}

# 	progressbar = yes

# 	weight = 10
# 	should_be_pinned_by_default = yes
# }

# je_panama_canal = {
# 	icon = "gfx/interface/icons/event_icons/event_map.dds"

# 	group = je_group_foreign_affairs

# 	complete = {
# 		any_scope_state = {
# 			state_region = s:STATE_PANAMA
# 			any_scope_building = {
# 				is_building_type = building_panama_canal
# 				occupancy >= 0.9
# 			}
# 		}
# 	}

# 	on_complete = {
# 		set_global_variable = panama_canal_complete
# 	}
# 	should_be_pinned_by_default = yes
# }