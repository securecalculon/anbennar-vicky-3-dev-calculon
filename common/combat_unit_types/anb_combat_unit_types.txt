﻿###
### Keep "unit tiers" per group in ascending order; the system will determine the default unit type for a country
### by the last defined unit type that it can build
###

### ARMY

### Infantry

combat_unit_type_undead_infantry = {
    group = combat_unit_group_infantry

    battle_modifier = {
        unit_offense_add = 10
        unit_defense_add = 10
        unit_morale_loss_add = 0
    }
	
    upkeep_modifier = {
		goods_input_magical_reagents_add = 0.5
		goods_input_meat_add = 1
    }

    can_build_conscript = {
        always = no
    }
	
	unlocking_technologies = { necromantic_magical_tradition }

	combat_unit_image = {
        texture = "gfx/unit_illustrations/infantry_undead.dds"
    }

}

combat_unit_type_behemoth_infantry = {
    group = combat_unit_group_infantry

    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }

    upkeep_modifier = {
        goods_input_small_arms_add = 6
        goods_input_ammunition_add = 3.5
        goods_input_artificery_doodads_add = 3
        goods_input_radios_add = 2
        goods_input_tanks_add = 3
    }
    
    battle_modifier = {
        unit_offense_add = 65
        unit_defense_add = 65
        unit_morale_loss_add = 4
        unit_devastation_mult = 0.2
    }

	unlocking_technologies = { mechs }

    combat_unit_image = {
		# fallback
        texture = "gfx/unit_illustrations/artillery_heavy_tank.dds"
    }
}

### Artillery

combat_unit_type_reactor_artillery = {
    group = combat_unit_group_artillery

    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_engines_add = 10
        goods_input_artillery_add = 7
		goods_input_ammunition_add = 5
        goods_input_radios_add = 2
        goods_input_artificery_doodads_add = 3
    }

    battle_modifier = {
        unit_offense_add = 90
        unit_defense_add = 40
        unit_morale_loss_add = 4
        unit_kill_rate_add = 0.3
        unit_morale_damage_mult = 0.2
        unit_devastation_mult = 0.4
    }

    formation_modifier = {
        military_formation_mobilization_speed_mult = -0.2
        military_formation_movement_speed_mult = -0.2
    }

	unlocking_technologies = { automatic_machine_guns }

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/attillery_reactor.dds"
    }
}

### Cavalry

combat_unit_type_turret_cavalry = {
    group = combat_unit_group_cavalry

    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_grain_add = 2
        goods_input_artillery_add = 2
    }

    battle_modifier = {
        unit_offense_add = 35
        unit_defense_add = 15
        unit_morale_loss_add = 8
        unit_occupation_mult = 0.2
        #should get modifiers to mobile/surprise battle conditions appearing
    }

	unlocking_technologies = { general_staff }

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/cavalry_eu_dragoon.dds"
    }

    upgrades = {
        combat_unit_type_war_rigs
		combat_unit_type_mobile_fortress
        combat_unit_type_light_tanks
		combat_unit_type_interceptor_castle
		combat_unit_type_holohana_frame
    }
}

combat_unit_type_war_rigs = {
    group = combat_unit_group_cavalry
	
    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_damestear_add = 0.5
        goods_input_small_arms_add = 1
		goods_input_automobiles_add = 1
    }

    battle_modifier = {
        unit_offense_add = 45
        unit_defense_add = 20
        unit_morale_loss_add = 8
        unit_morale_loss_mult = 0.05
        unit_occupation_mult = 0.4
    }
	
	formation_modifier = {
        military_formation_mobilization_speed_mult = 0.25
		military_formation_movement_speed_mult = 0.25
    }

	unlocking_technologies = { breech_loading_artillery }

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/cavalry_light_tank.dds"
    }

    upgrades = {
		combat_unit_type_mobile_fortress
		combat_unit_type_light_tanks
		combat_unit_type_interceptor_castle
		combat_unit_type_holohana_frame
    }
}

combat_unit_type_mobile_fortress = {
    group = combat_unit_group_cavalry
	
    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_engines_add = 2
        goods_input_small_arms_add = 2
		goods_input_ammunition_add = 2
		goods_input_artificery_doodads_add = 1
    }

    battle_modifier = {
        unit_offense_add = 30
        unit_defense_add = 50
        unit_morale_loss_add = 8
    }

	unlocking_technologies = { breech_loading_artillery }
    
	formation_modifier = {
		military_formation_movement_speed_mult = -0.1
    }

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/artillery_heavy_tank.dds"
    }

    upgrades = {
        combat_unit_type_war_rigs
        combat_unit_type_light_tanks
		combat_unit_type_interceptor_castle
		combat_unit_type_holohana_frame
    }
}

combat_unit_type_interceptor_castle = {
    group = combat_unit_group_cavalry
	
    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_engines_add = 4
        goods_input_small_arms_add = 3
		goods_input_ammunition_add = 3
		goods_input_artificery_doodads_add = 2
    }

    battle_modifier = {
        unit_offense_add = 35
        unit_defense_add = 70
        unit_morale_loss_add = 6
        unit_kill_rate_add = 0.05
        unit_occupation_mult = 0.2
		unit_devastation_mult = 0.1
    }

	unlocking_technologies = { defense_in_depth }
    

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/artillery_heavy_tank.dds"
    }
	
	formation_modifier = {
		military_formation_movement_speed_mult = 0.1
    }

    upgrades = {
        combat_unit_type_war_rigs
        combat_unit_type_light_tanks
		combat_unit_type_holohana_frame
    }
}

combat_unit_type_holohana_frame = {
    group = combat_unit_group_cavalry
	
    can_build_conscript = {
        NOT = {
            has_law = law_type:law_peasant_levies
        }
    }
    
    upkeep_modifier = {
        goods_input_tanks_add = 4
        goods_input_small_arms_add = 4
		goods_input_ammunition_add = 4
		goods_input_artificery_doodads_add = 4
		goods_input_radios_add = 2
    }

    battle_modifier = {
        unit_offense_add = 40
        unit_defense_add = 90
        unit_morale_loss_add = 4
        unit_kill_rate_add = 0.1
        unit_occupation_mult = 0.3
		unit_devastation_mult = 0.1
    }

	unlocking_technologies = { mechs }
    

    combat_unit_image = {
        # fallback
        texture = "gfx/unit_illustrations/artillery_heavy_tank.dds"
    }
	
	formation_modifier = {
		military_formation_mobilization_speed_mult = 0.2
		military_formation_movement_speed_mult = 0.2
    }

    upgrades = {
        combat_unit_type_war_rigs
        combat_unit_type_light_tanks
    }
}

### NAVY

### Light ship group

### Flagship group

### Support Ship group