﻿company_qazhraz = {
	icon = "gfx/interface/icons/company_icons/basic_agriculture_2.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_rice.dds"

	flavored_company = yes

	building_types = {
		building_livestock_ranch
		building_food_industry
	}

	potential = {
		has_interest_marker_in_region = region_adzalas_gulf
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_ZULBUR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_ZULBUR
			any_scope_building = {
				is_building_type = building_livestock_ranch
				level >=10
			}
		}
	}

	prosperity_modifier = {
		unit_supply_consumption_mult = -0.1
		country_military_goods_cost_mult = -0.05
	}

	ai_weight = {
		value = 3
	}
}
