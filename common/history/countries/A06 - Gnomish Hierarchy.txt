COUNTRIES = {
	c:A06 ?= {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_1_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_technocracy	
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience	
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_exploitation
	    activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools #Not allowed public schools with State religion
		# No healthcare
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without feminism
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice

		set_institution_investment_level = {
			institution = institution_schools 
            level = 3
		}
		
	}
}