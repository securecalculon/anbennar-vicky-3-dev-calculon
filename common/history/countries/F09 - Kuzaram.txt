﻿COUNTRIES = {
	c:F09 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic
	}
}