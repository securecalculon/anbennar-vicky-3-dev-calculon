﻿COUNTRIES = {
	c:B19 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience #Pro-Ravelian but not supremacist either
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No Police force, Local Police Force in doc
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system # They rely on Ravelians a lot
		activate_law = law_type:law_nation_of_artifice # They are meant to be scholars
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance

		set_tariffs_export_priority = g:fabric #Effective for the entire Customs Union
		set_tariffs_import_priority = g:dye

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}
	}
}